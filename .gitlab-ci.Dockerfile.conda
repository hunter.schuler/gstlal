# This is a Dockerfile to build a conda GstLAL 
# runtime container. 

ARG CI_REGISTRY_IMAGE
ARG CI_COMMIT_REF_NAME
ARG CONDA_ENV

# Build stage
FROM $CI_REGISTRY_IMAGE/dependencies/conda-$CONDA_ENV:$CI_COMMIT_REF_NAME AS build

ARG CI_REGISTRY_IMAGE
ARG CI_COMMIT_REF_NAME
ARG CONDA_ENV

# Labeling/packaging stuff:
LABEL name="GstLAL Runtime Package, conda" \
      maintainer="Patrick Godwin <patrick.godwin@ligo.org>" \
      date="2021-10-14"

# Copy source to container
COPY gstlal /gstlal
COPY gstlal-ugly /gstlal-ugly
COPY gstlal-calibration /gstlal-calibration
COPY gstlal-inspiral /gstlal-inspiral
COPY gstlal-burst /gstlal-burst

# Make RUN commands use bash:
SHELL ["/bin/bash", "-c"]

# Install gstlal
RUN . /opt/conda/etc/profile.d/conda.sh && \
    conda activate gstlal-$CONDA_ENV && \
    export PREFIX="$CONDA_PREFIX" && \
    export CONDA_BUILD="1" && \
    conda activate gstlal-$CONDA_ENV && \
    cd /gstlal && \
    ./00init.sh && \
    ./configure --prefix=$CONDA_PREFIX --without-doxygen && \
    make -j$NUM_CORES && \
    make install -j$NUM_CORES
RUN rm -rf gstlal

# Install gstlal-ugly
RUN . /opt/conda/etc/profile.d/conda.sh && \
    conda activate gstlal-$CONDA_ENV && \
    export PREFIX="$CONDA_PREFIX" && \
    export CONDA_BUILD="1" && \
    conda activate gstlal-$CONDA_ENV && \
    cd /gstlal-ugly && \
    ./00init.sh && \
    ./configure --prefix=$CONDA_PREFIX --without-doxygen && \
    make -j$NUM_CORES && \
    make install -j$NUM_CORES
RUN rm -rf gstlal-ugly

# Install gstlal-burst
RUN . /opt/conda/etc/profile.d/conda.sh && \
    conda activate gstlal-$CONDA_ENV && \
    export PREFIX="$CONDA_PREFIX" && \
    export CONDA_BUILD="1" && \
    conda activate gstlal-$CONDA_ENV && \
    cd /gstlal-burst && \
    ./00init.sh && \
    ./configure --prefix=$CONDA_PREFIX --without-doxygen && \
    make -j$NUM_CORES && \
    make install -j$NUM_CORES
RUN rm -rf gstlal-burst


# Install gstlal-calibration
RUN . /opt/conda/etc/profile.d/conda.sh && \
    conda activate gstlal-$CONDA_ENV && \
    export PREFIX="$CONDA_PREFIX" && \
    export CONDA_BUILD="1" && \
    conda activate gstlal-$CONDA_ENV && \
    cd /gstlal-calibration && \
    ./00init.sh && \
    ./configure --prefix=$CONDA_PREFIX --without-doxygen && \
    make -j$NUM_CORES && \
    make install -j$NUM_CORES
RUN rm -rf gstlal-calibration


# Install gstlal-inspiral
RUN . /opt/conda/etc/profile.d/conda.sh && \
    conda activate gstlal-$CONDA_ENV && \
    export PREFIX="$CONDA_PREFIX" && \
    export CONDA_BUILD="1" && \
    conda activate gstlal-$CONDA_ENV && \
    cd /gstlal-inspiral && \
    ./00init.sh && \
    ./configure --prefix=$CONDA_PREFIX --without-doxygen --disable-massmodel && \
    make -j$NUM_CORES && \
    make install -j$NUM_CORES
RUN rm -rf gstlal-inspiral

# Run stage
FROM $CI_REGISTRY_IMAGE/dependencies/conda-$CONDA_ENV:$CI_COMMIT_REF_NAME AS run

ARG CONDA_ENV

# Replace created environment with the base environment
# Also grab conda global config settings
COPY --from=build /opt/conda /opt/conda
COPY --from=build /opt/conda/.condarc /opt/conda

# Set up entrypoint
COPY .gitlab-ci.conda_entrypoint.sh /bin/entrypoint.sh
RUN chmod +x /bin/entrypoint.sh

# Export environment variables
ENV PKG_CONFIG_PATH /opt/conda/envs/gstlal-$CONDA_ENV/lib/pkgconfig
ENV GST_PLUGIN_PATH /opt/conda/envs/gstlal-$CONDA_ENV/lib/gstreamer-1.0
ENV GSTLAL_FIR_WHITEN 0
ENV TMPDIR /tmp

# Give entrypoint knowledge about environment to source
ENV CONDA_ENV $CONDA_ENV

# Setup for interactive shell sessions (without polluting $HOME)
RUN echo ". /opt/conda/etc/profile.d/conda.sh && conda activate gstlal-${CONDA_ENV}" >> /root/.bashrc

ENTRYPOINT ["/bin/entrypoint.sh"]
CMD ["/bin/bash"]

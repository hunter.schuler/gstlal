/*
 * Copyright (C) 2021  Aaron Viets <aaron.viets@ligo.org>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __GSTLAL_CALIBCORR_H__
#define __GSTLAL_CALIBCORR_H__


#include <complex.h>

#include <glib.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>

#include <fftw3.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_permutation.h>


G_BEGIN_DECLS


/*
 * lal_calibcorr element
 */


#define GSTLAL_CALIBCORR_TYPE \
	(gstlal_calibcorr_get_type())
#define GSTLAL_CALIBCORR(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj), GSTLAL_CALIBCORR_TYPE, GSTLALCalibCorr))
#define GSTLAL_CALIBCORR_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GSTLAL_CALIBCORR_TYPE, GSTLALCalibCorrClass))
#define GST_IS_GSTLAL_CALIBCORR(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj), GSTLAL_CALIBCORR_TYPE))
#define GST_IS_GSTLAL_CALIBCORR_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GSTLAL_CALIBCORR_TYPE))


typedef struct _GSTLALCalibCorr GSTLALCalibCorr;
typedef struct _GSTLALCalibCorrClass GSTLALCalibCorrClass;


/**
 * GSTLALCalibCorr:
 */


struct _GSTLALCalibCorr {
	GstBaseSink basesink;

	/* stream info */
	gint rate;
	gint unit_size;
	gint channels;

	/* timestamp bookkeeping */
	GstClockTime t0;
	GstClockTime pts;
	guint64 offset0;
	guint64 next_in_offset;

	/* element parameters */
	gboolean need_update;

	GMutex cinv_lock;
	GMutex tst_lock;
	GMutex pum_lock;
	GMutex uim_lock;

	double ktst;
	double tau_tst;
	double kpum;
	double tau_pum;
	double kuim;
	double tau_uim;
	double kc;
	double fcc;
	double fs_squared;
	double fs_over_Q;

	double *freqs;
	gint num_freqs;

	complex double *R_tdcf;
	complex double *tst_over_R_tdcf;
	complex double *pum_over_R_tdcf;
	complex double *uim_over_R_tdcf;

	/* properties */
	double *cinv_freqs;
	gint num_cinv_freqs;
	gint cinv_sr;
	gint cinv_fir_length;
	gint cinvcorr_tf_length;
	complex double *cres_model;
	complex double *cinv;
	gint cres_model_length;
	complex double *measured_response;
	gint *cinv_indices;
	gint measured_response_length;
	complex double *cinvcorr_tf;
	double *cinvcorr_filt;
	double *cinv_window;

	complex double *D;
	gint D_length;

	double *tst_freqs;
	gint num_tst_freqs;
	gint tst_sr;
	gint tst_fir_length;
	gint tstcorr_tf_length;
	complex double *Ftst_model;
	gint Ftst_model_length;
	complex double *tst_model;
	complex double *tst;
	gint tst_model_length;
	complex double *tst_tf;
	gint *tst_indices;
	gint tst_tf_length;
	complex double *tstcorr_tf;
	double *tstcorr_filt;
	double *tst_window;

	double *pum_freqs;
	gint num_pum_freqs;
	gint pum_sr;
	gint pum_fir_length;
	gint pumcorr_tf_length;
	complex double *Fpum_model;
	gint Fpum_model_length;
	complex double *pum_model;
	complex double *pum;
	gint pum_model_length;
	complex double *pum_tf;
	gint *pum_indices;
	gint pum_tf_length;
	complex double *pumcorr_tf;
	double *pumcorr_filt;
	double *pum_window;

	double *uim_freqs;
	gint num_uim_freqs;
	gint uim_sr;
	gint uim_fir_length;
	gint uimcorr_tf_length;
	complex double *Fuim_model;
	gint Fuim_model_length;
	complex double *uim_model;
	complex double *uim;
	gint uim_model_length;
	complex double *uim_tf;
	gint *uim_indices;
	gint uim_tf_length;
	complex double *uimcorr_tf;
	double *uimcorr_filt;
	double *uim_window;

	char *filename;
};


/**
 * GSTLALCalibCorrClass:
 * @parent_class:  the parent class
 */


struct _GSTLALCalibCorrClass {
	GstBaseSinkClass parent_class;
};


GType gstlal_calibcorr_get_type(void);


G_END_DECLS


#endif	/* __GSTLAL_CALIBCORR_H__ */

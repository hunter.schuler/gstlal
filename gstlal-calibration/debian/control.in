Source: gstlal-calibration
Maintainer: Madeline Wade <madeline.wade@gravity.phys.uwm.edu>
Section: lscsoft
Priority: optional
Standards-Version: 3.9.2
X-Python3-Version: >= @MIN_PYTHON_VERSION@
Build-Depends:
 debhelper (>= 9),
 dh-python,
 fakeroot,
 gstlal-dev (>= @MIN_GSTLAL_VERSION@),
 liblal-dev (>= @MIN_LAL_VERSION@),
 liblalmetaio-dev (>= @MIN_LALMETAIO_VERSION@),
 libgstreamer1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 pkg-config (>= @MIN_PKG_CONFIG_VERSION@),
 python3-all-dev (>= @MIN_PYTHON_VERSION@),
 python3-numpy

Package: gstlal-calibration
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends},
 gir1.2-glib-2.0,
 gir1.2-gstreamer-1.0 (>= @MIN_GSTREAMER_VERSION@),
 gir1.2-gst-plugins-base-1.0 (>= @MIN_GSTREAMER_VERSION@),
 gstlal (>= @MIN_GSTLAL_VERSION@),
 gstlal-ugly (>= @MIN_GSTLALUGLY_VERSION@),
 gstreamer1.0-plugins-base (>= @MIN_GSTREAMER_VERSION@),
 gstreamer1.0-plugins-good (>= @MIN_GSTREAMER_VERSION@),
 gstreamer1.0-tools (>= @MIN_GSTREAMER_VERSION@),
 lal (>= @MIN_LAL_VERSION@),
 lalmetaio (>= @MIN_LALMETAIO_VERSION@),
 libgirepository-1.0-1,
 libgstreamer1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 python3 (>= @MIN_PYTHON_VERSION@),
 python3-gi,
 python3-ligo-segments (>= @MIN_LIGO_SEGMENTS_VERSION@),
 python3-gst-1.0,
 python3-numpy,
 python3-scipy
Conflicts:
 gstlal-ugly (<< 0.6.0)
Description: GStreamer for GW data analysis (calibration parts)
 This package contains the plugins and shared libraries required to run the gstlal calibration software.

Package: gstlal-calibration-dev
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends},
 gstlal-calibration (= ${binary:Version}),
 gstlal-dev (>= @MIN_GSTLAL_VERSION@),
 liblal-dev (>= @MIN_LAL_VERSION@),
 liblalmetaio-dev (>= @MIN_LALMETAIO_VERSION@),
 libgstreamer1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 python3-all-dev (>= @MIN_PYTHON_VERSION@),
 gsl-dev
Conflicts:
 gstlal-ugly (<< 0.6.0),
 gstlal-calibration (<< 1.3.0)
Description: GStreamer for GW data analysis (calibration parts, developer package)
 This package contains the files needed for building gstlal-calibration based
 plugins and programs.

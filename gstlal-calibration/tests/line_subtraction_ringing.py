#!/usr/bin/env python3
# Copyright (C) 2021  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import numpy as np
import os
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import matplotlib.pyplot as plt

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
GObject.threads_init()
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlal import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlal import test_common


from gstlal import FIRtools as fir
from ticks_and_grid import ticks_and_grid


# Constants
freq = 10.3		# Hz
test_duration = 400000	# seconds
amplitude_mod = 0.000
phase_mod = 0.000
avg_time = 512		# seconds
other_lines = [10.301, 10.295, 10.31, 10.28]	# Hz
other_mod = 0.000
fft_length = 16000 * 64
fft_overlap = 8000 * 64
num_ffts = (test_duration - 1000 - 8000) // (16000 - 8000)
tf_length = fft_length // 2 + 1

#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


def line_subtraction_ringing_01(pipeline, name):
	#
	# This test generates fake noisy data with sinusoidal excitations that
	# are subtracted using gstlal-calibration software and plots it in order
	# to test for the presence of "ringing" in very nearby frequency bands.
	#

	# The noise floor
	noise = test_common.test_src(pipeline, rate = 64, test_duration = test_duration, wave = 5, src_suffix = '0')
	noise = pipeparts.mktee(pipeline, noise)

	# The witness channel used to subtract the line, in this case a pure sinusoid.
	witness = test_common.test_src(pipeline, rate = 64, test_duration = test_duration, wave = 0, freq = freq, src_suffix = '1')

	# The signal in the data, which may not be a pure sinusoid, but could have amplitude and/or phase modulation.
	amplitude = test_common.test_src(pipeline, rate = 16, test_duration = test_duration, wave = 0, src_suffix = '2', volume = 0)
	amplitude = pipeparts.mkgeneric(pipeline, amplitude, "lal_add_constant", value = 100)
	# Slow modulation (changes on ~2500s timescale)
	amp_mod = test_common.test_src(pipeline, rate = 16, test_duration = test_duration, wave = 5, src_suffix = '3')
	amp_mod = pipeparts.mkgeneric(pipeline, amp_mod, "lal_smoothkappas", array_size = 1, avg_array_size = 16 * 2500, default_kappa_re = 0)
	amp_mod = pipeparts.mkaudioamplify(pipeline, amp_mod, amplitude_mod * 4 * 50 * 100)
	amplitude = calibration_parts.mkadder(pipeline, calibration_parts.list_srcs(pipeline, amplitude, amp_mod))
	amplitude = pipeparts.mktogglecomplex(pipeline, pipeparts.mkmatrixmixer(pipeline, amplitude, matrix = [[1, 0]]))
	p_mod = test_common.test_src(pipeline, rate = 16, test_duration = test_duration, wave = 5, src_suffix = '4')
	# Slow modulation (changes on ~2500s timescale)
	p_mod = pipeparts.mkgeneric(pipeline, p_mod, "lal_smoothkappas", array_size = 1, avg_array_size = 16 * 2500, default_kappa_re = 0)
	p_mod = pipeparts.mktogglecomplex(pipeline, pipeparts.mkmatrixmixer(pipeline, p_mod, matrix = [[0.0, phase_mod * 4 * 50]]))
	signal = calibration_parts.mkmultiplier(pipeline, calibration_parts.list_srcs(pipeline, amplitude, pipeparts.mkgeneric(pipeline, p_mod, "cexp")))
	signal = calibration_parts.mkresample(pipeline, signal, 4, False, "audio/x-raw, format=Z128LE, rate=64")
	signal = pipeparts.mkgeneric(pipeline, signal, "lal_demodulate", line_frequency = -1.0 * freq, prefactor_real = -2.0)
	signal = pipeparts.mkcapsfilter(pipeline, signal, "audio/x-raw, format=Z128LE, rate=64")
	signal = pipeparts.mkgeneric(pipeline, signal, "creal")
	signal = pipeparts.mkcapsfilter(pipeline, signal, "audio/x-raw, format=F64LE, rate=64")

	# Add noise to signal
	strain = pipeparts.mktee(pipeline, calibration_parts.mkadder(pipeline, calibration_parts.list_srcs(pipeline, signal, noise)))

	# If requested, add other lines into the spectrum that are not present in the witness
	for line_freq in other_lines:
		# The other line in the data, which may not be a pure sinusoid, but could have amplitude and/or phase modulation.
		amplitude = test_common.test_src(pipeline, rate = 16, test_duration = test_duration, wave = 0, src_suffix = 'ampAt%fHz' % line_freq, volume = 0)
		amplitude = pipeparts.mkgeneric(pipeline, amplitude, "lal_add_constant", value = 1)
		# Slow modulation (changes on ~2500s timescale)
		amp_mod = test_common.test_src(pipeline, rate = 16, test_duration = test_duration, wave = 5, src_suffix = 'ampmodAt%fHz' % line_freq)
		amp_mod = pipeparts.mkgeneric(pipeline, amp_mod, "lal_smoothkappas", array_size = 1, avg_array_size = 16 * 2500, default_kappa_re = 0)
		amp_mod = pipeparts.mkaudioamplify(pipeline, amp_mod, other_mod * 4 * 50 * 1)
		amplitude = calibration_parts.mkadder(pipeline, calibration_parts.list_srcs(pipeline, amplitude, amp_mod))
		amplitude = pipeparts.mktogglecomplex(pipeline, pipeparts.mkmatrixmixer(pipeline, amplitude, matrix = [[1, 0]]))
		p_mod = test_common.test_src(pipeline, rate = 16, test_duration = test_duration, wave = 5, src_suffix = 'pmodAt%fHz' % line_freq)
		# Slow modulation (changes on ~2500s timescale)
		p_mod = pipeparts.mkgeneric(pipeline, p_mod, "lal_smoothkappas", array_size = 1, avg_array_size = 16 * 2500, default_kappa_re = 0)
		p_mod = pipeparts.mktogglecomplex(pipeline, pipeparts.mkmatrixmixer(pipeline, p_mod, matrix = [[0.0, other_mod * 4 * 50]]))
		signal = calibration_parts.mkmultiplier(pipeline, calibration_parts.list_srcs(pipeline, amplitude, pipeparts.mkgeneric(pipeline, p_mod, "cexp")))
		signal = calibration_parts.mkresample(pipeline, signal, 4, False, "audio/x-raw, format=Z128LE, rate=64")
		signal = pipeparts.mkgeneric(pipeline, signal, "lal_demodulate", line_frequency = -1.0 * line_freq, prefactor_real = -2.0)
		signal = pipeparts.mkcapsfilter(pipeline, signal, "audio/x-raw, format=Z128LE, rate=64")
		signal = pipeparts.mkgeneric(pipeline, signal, "creal")
		signal = pipeparts.mkcapsfilter(pipeline, signal, "audio/x-raw, format=F64LE, rate=64")

		# Add signal to strain
		strain = pipeparts.mktee(pipeline, calibration_parts.mkadder(pipeline, calibration_parts.list_srcs(pipeline, strain, signal)))

	# Subtract signal using same function that is used in gstlal_compute_strain
	clean_strain = calibration_parts.remove_lines_with_witnesses(pipeline, strain, [[witness]], [[freq]], [0], [], filter_latency = 1, compute_rate = 16, rate_out = 64, num_median = 1 * 16, num_avg = avg_time * 16, noisesub_gate_bit = None)

	#strain = calibration_parts.mkresample(pipeline, strain, 4, False, "audio/x-raw, format=F64LE, rate=64")
	#clean_strain = calibration_parts.mkresample(pipeline, clean_strain, 4, False, "audio/x-raw, format=F64LE, rate=64")
	# Remove the initial data
	strain = calibration_parts.mkinsertgap(pipeline, strain, insert_gap = False, chop_length = 600 * 1000000000)
	clean_strain = calibration_parts.mkinsertgap(pipeline, clean_strain, insert_gap = False, chop_length = 600 * 1000000000)
	strain = pipeparts.mktee(pipeline, pipeparts.mkprogressreport(pipeline, strain, "strain"))
	clean_strain = pipeparts.mktee(pipeline, pipeparts.mkprogressreport(pipeline, clean_strain, "clean_strain"))
	# Compute ASDs
	pipeparts.mkgeneric(pipeline, strain, "lal_asd", fft_samples = 16000 * 64, overlap_samples = 8000 * 64, window_type = 3, filename = "strainASD_%davg.txt" % avg_time)
	pipeparts.mkgeneric(pipeline, clean_strain, "lal_asd", fft_samples = 16000 * 64, overlap_samples = 8000 * 64, window_type = 3, filename = "clean_strainASD_%davg.txt" % avg_time)

	# Compute transfer function
	interleaved = calibration_parts.mkinterleave(pipeline, [clean_strain, strain])
	pipeparts.mkgeneric(pipeline, interleaved, "lal_transferfunction", fft_length = fft_length, fft_overlap = fft_overlap, num_ffts = num_ffts, use_median = False, update_samples = 1e15, fft_window_type = 3, filename = "lineSubRingingTF_%davg.txt" % avg_time)

	#
	# done
	#
	
	return pipeline
	
#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(line_subtraction_ringing_01, "line_subtraction_ringing_01")

# Get ASD data
print("Loading ASD txt file 1 of 2")
strain = np.loadtxt("strainASD_%davg.txt" % avg_time)
print("Done loading ASD txt file 1 of 2")
fvec = np.transpose(strain)[0]
strain = np.transpose(strain)[1]
print("Loading ASD txt file 2 of 2")
clean = np.loadtxt("clean_strainASD_%davg.txt" % avg_time)
print("Done loading ASD txt file 2 of 2")
clean = np.transpose(clean)[1]

# Get TF data
print("Loading TF txt file")
# Remove unwanted lines from TF file, and re-format wanted lines
f = open("lineSubRingingTF_%davg.txt" % avg_time, "r")
lines = f.readlines()
f.close()
os.system("rm lineSubRingingTF_%davg.txt" % avg_time)
f = open("lineSubRingingTF_%davg.txt" % avg_time, "w")
for j in range(3, 3 + tf_length):
	f.write(lines[j].replace(' + ', '\t').replace(' - ', '\t-').replace('i', ''))
f.close()

TF = np.loadtxt("lineSubRingingTF_%davg.txt" % avg_time)
print("Done loading TF txt file")

TFfvec = []
TFmag = []
TFphase = []
for j in range(0, len(TF)):
	TFfvec.append(TF[j][0])
	tf_at_f = (TF[j][1] + 1j * TF[j][2])
	TFmag.append(abs(tf_at_f))
	TFphase.append(np.angle(tf_at_f) * 180.0 / np.pi)

# Plot ASDs against frequency
plt.figure(figsize = (10, 6))
colors = ['red', 'blue']
labels = ['strain', 'clean']
plt.plot(fvec, strain, colors[0], linewidth = 0.75)
plt.plot(fvec, clean, colors[1], linewidth = 0.75)
patches = [mpatches.Patch(color = colors[j], label = r'$%s$' % labels[j]) for j in range(len(labels))]
plt.legend(handles = patches, loc = 'upper right', ncol = 1)
plt.title('ASD with %d s Average' % (avg_time))
plt.ylabel(r'${\rm ASD}\ \left[{\rm strain / }\sqrt{\rm Hz}\right]$')
plt.xlabel(r'${\rm Frequency \ [Hz]}$')
ticks_and_grid(plt.gca(), xmin = freq - 0.03, xmax = freq + 0.02, ymin = 0.0001, ymax = 10000, xscale = 'linear', yscale = 'log')

if any(other_lines):
	plt.savefig("lineSubRingingOtherLinesASD_%davg.png" % (avg_time))
else:
	plt.savefig("lineSubRingingASD_%davg.png" % (avg_time))

# Plot transfer function against frequency
plt.figure(figsize = (10, 10))
plt.subplot(211)
plt.plot(TFfvec, TFmag, 'green', linewidth = 0.75)
ticks_and_grid(plt.gca(), xmin = freq - 0.03, xmax = freq + 0.02, ymin = 0, ymax = 2, xscale = 'linear', yscale = 'linear')
plt.ylabel('Magnitude')
plt.title('TF with %d s Average' % (avg_time))
plt.subplot(212)
plt.plot(TFfvec, TFphase, 'green', linewidth = 0.75)
ticks_and_grid(plt.gca(), xmin = freq - 0.03, xmax = freq + 0.02, ymin = -180, ymax = 180, xscale = 'linear', yscale = 'linear')
plt.ylabel('Phase [deg]')
plt.xlabel('Frequency [Hz]')

if any(other_lines):
	plt.savefig("lineSubRingingOtherLinesTF_%davg.png" % (avg_time))
else:
	plt.savefig("lineSubRingingTF_%davg.png" % (avg_time))



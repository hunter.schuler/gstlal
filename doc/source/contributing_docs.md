# Contributing Documentation

This guide assumes the reader has read the [Contribution workflow](contributing.md) for details about making changes to
code within gstlal repo, since the documentation files are updated by a similar workflow.

## Writing Documentation

In general, the gstlal documentation uses [RestructuredText (rst)](https://docutils.sourceforge.io/rst.html) files
ending in `.rst` or [Markdown](https://www.markdownguide.org/basic-syntax/) files ending in `.md`.

The documentation files for gstlal are located under `gstlal/doc/source`. If you add a new page (doc file), make sure to
reference it from the main index page.

Useful Links:

- [MyST Directive Syntax](https://myst-parser.readthedocs.io/en/latest/syntax/syntax.html#syntax-directives)


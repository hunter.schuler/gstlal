# GstLAL Conda Environment Workflow

Since solving a dependency problem can be NP-hard, and could therefore take considerable time to complete, we store our
production conda environments as fully-specified (or "qualified")
lock files, which include version numbers and all packages. These will be called "{name}-{os}.lock"
with other details in the name to identify different purposes.

This workflow now leverages conda-flow, see: https://git.ligo.org/james.kennington/conda-flow

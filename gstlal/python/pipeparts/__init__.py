# Copyright (C) 2009--2013  LIGO Scientific Collaboration
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

__author__ = "Kipp Cannon <kipp.cannon@ligo.org>, Chad Hanna <chad.hanna@ligo.org>, Drew Keppel <drew.keppel@ligo.org>"
__version__ = "FIXME"
__date__ = "FIXME"


__doc__ = """The `pipeparts` module contains all of the *elements* used to construct gstreamer pipelines
for gravitational wave analysis. These elements are grouped into thematic submodules.
"""


# Flatten package to preserve import paths
from .sink import AppSync as AppSync
from .sink import BYTE_ORDER as BYTE_ORDER
from .transform import audioresample_variance_gain as audioresample_variance_gain
from .sink import ConnectAppsinkDumpDot as connect_appsink_dump_dot
from .mux import FrameCPPChannelDemuxCheckSegmentsHandler as framecpp_channeldemux_check_segments
from .mux import FrameCPPChannelDemuxSetUnitsHandler as framecpp_channeldemux_set_units
from .sink import framecpp_filesink_cache_entry_from_mfs_message as framecpp_filesink_cache_entry_from_mfs_message
from .sink import framecpp_filesink_ldas_path_handler as framecpp_filesink_ldas_path_handler
from .transform import abs_ as mkabs
from .transform import adder as mkadder
from .sink import app as mkappsink
from .transform import amplify as mkaudioamplify
from .filters import audio_cheb_band as mkaudiochebband
from .filters import audio_cheb_limit as mkaudiocheblimit
from .transform import audio_convert as mkaudioconvert
from .transform import audio_rate as mkaudiorate
from .source import audio_test as mkaudiotestsrc
from .transform import undersample as mkaudioundersample
from .sink import auto_audio as mkautoaudiosink
from .transform import auto_chisq as mkautochisq
from .mux import avi_mux as mkavimux
from .transform import bit_vector_gen as mkbitvectorgen
from .trigger import blcbc_trigger_gen as mkblcbctriggergen
from .trigger import burst_trigger_gen as mkbursttriggergen
from .filters import caps as mkcapsfilter
from .transform import set_caps as mkcapssetter
from .plot import channelgram as mkchannelgram
from .transform import check_timestamps as mkchecktimestamps
from .transform import clean as mkclean
from .transform import colorspace as mkcolorspace
from .transform import mkcomputegamma as mkcomputegamma
from .transform import deglitch as mkdeglitcher
from .transform import denoise as mkdenoiser
from .filters import drop as mkdrop
from .source import fake_ligo as mkfakeLIGOsrc
from .source import fake_aligo as mkfakeadvLIGOsrc
from .source import fake_avirgo as mkfakeadvvirgosrc
from .sink import fake as mkfakesink
from .source import fake as mkfakesrc
from .sink import file as mkfilesink
from .transform import fir_bank as mkfirbank
from .filters import fir as mkfirfilter
from .encode import flac as mkflacenc
from .mux import framecpp_channel_demux as mkframecppchanneldemux
from .mux import framecpp_channel_mux as mkframecppchannelmux
from .mux import framecpp_channel_mux_from_list as mkframecppchannelmux_from_list
from .sink import gwf as mkframecppfilesink
from .source import framexmit as mkframexmitsrc
from .filters import gate as mkgate
from .pipetools import make_element_with_src as mkgeneric
from .plot import histogram as mkhistogram
from .encode import igwd_parse as mkigwdparse
from .filters import iir as mkiirfilter
from .filters import inject as mkinjections
from .transform import interpolator as mkinterpolator
from .trigger import itac as mkitac
from .trigger import itacac as mkitacac
from .source import cache as mklalcachesrc
from .transform import latency as mklatency
from .transform import lho_coherent_null as mklhocoherentnull
from .source import lvshm as mklvshmsrc
from .transform import matrix_mixer as mkmatrixmixer
from .transform import mean as mkmean
from .sink import multi_file as mkmultifilesink
from .transform import multiplier as mkmultiplier
from .source import nds as mkndssrc
from .filters import remove_fake_disconts as mknofakedisconts
from .encode import tsv as mknxydump
from .sink import tsv as mknxydumpsink
from .sink import tsv_tee as mknxydumpsinktee
from .transform import mkodctodqv as mkodctodqv
from .mux import ogg_mux as mkoggmux
from .sink import ogm_video as mkogmvideosink
from .transform import peak as mkpeak
from .sink import playback as mkplaybacksink
from .transform import pow as mkpow
from .transform import progress_report as mkprogressreport
from .transform import queue as mkqueue
from .transform import reblock as mkreblock
from .transform import resample as mkresample
from .source import segment as mksegmentsrc
from .transform import shift as mkshift
from .plot import spectrum as mkspectrumplot
from .filters import state_vector as mkstatevector
from .transform import sum_squares as mksumsquares
from .transform import tag_inject as mktaginject
from .sink import tcp_server as mktcpserversink
from .transform import td_whiten as mktdwhiten
from .transform import tee as mktee
from .encode import theora as mktheoraenc
from .transform import toggle_complex as mktogglecomplex
from .trigger import trigger as mktrigger
from .trigger import trigger_gen as mktriggergen
from .sink import trigger_xml_writer as mktriggerxmlwritersink
from .transform import trim as mktrim
from .encode import uri_decode_bin as mkuridecodebin
from .sink import auto_video as mkvideosink
from .encode import vorbis as mkvorbisenc
from .encode import wav as mkwavenc
from .transform import whiten as mkwhiten
from .source import SrcDeferredLink as src_deferred_link
from .pipedot import to_file as write_dump_dot

"""Unittests for plotting pipeparts"""
import pytest

from gstlal import gstpipetools
from gstlal.pipeparts import pipetools, plot
from gstlal.pipeparts.pipetools import Gst, GObject
from gstlal.utilities import testtools


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestPlot:
	"""Group test for encoders"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	@testtools.broken('Python gst plugins are broken')
	def test_channelgram(self, pipeline, src):
		"""Test channelgram"""
		elem = plot.channelgram(pipeline, src)
		assert_elem_props(elem, 'GstWavEnc')

	@testtools.broken('Python gst plugins are broken')
	def test_spectrum(self, pipeline, src):
		"""Test spectrum"""
		elem = plot.spectrum(pipeline, src)
		assert_elem_props(elem, 'GstWavEnc')

	@testtools.broken('Python gst plugins are broken')
	def test_histogram(self, pipeline, src):
		"""Test histogram"""
		elem = plot.histogram(pipeline, src)
		assert_elem_props(elem, 'GstWavEnc')

"""Unittests for ElementRegistry
"""
import collections
import inspect
import types
from unittest import mock

import pytest

from gstlal import pipeparts
from gstlal.utilities import element_registry


@pytest.mark.skip(reason="will fail due to function aliases in pipeparts, re-enable once ElementRegistry uses namespaced functions in pipeparts")
class TestMixinFormatting:
	"""Test group for mixin formatters"""

	def test_format_registered_stream_method(self):
		res = element_registry.RegisteredElement(pipeparts.mkchannelgram)
		assert res.format_method_source() == '\tdef channelgram(self, *srcs, **kwargs):\n\t\t"""Automatically generated pass-through method for function pipeparts.mkchannelgram"""\n\t\treturn self._attach_element(pipeparts.mkchannelgram, *srcs, **kwargs)'

	def test_generate_source(self):
		res = element_registry.generate_registered_method_source()
		assert isinstance(res, str)


@pytest.mark.skip(reason="will fail due to function aliases in pipeparts, re-enable once ElementRegistry uses namespaced functions in pipeparts")
class TestElementRegistry:
	"""Test group for element registry members"""
	_REGISTERED_MEMBERS = dict([(name, func) for name, func in inspect.getmembers(element_registry.ElementRegistry) if inspect.isfunction(func) and not name.startswith('_')])

	@pytest.mark.parametrize('element', element_registry.ALL_ELEMENTS)
	def test_element_registered(self, element: element_registry.RegisteredElement):
		"""Check that an element is registered and refers to the function"""
		# Check element defined in method
		assert element.name in self._REGISTERED_MEMBERS

		# Check method calls appropriate function (if source is available)
		try:
			impl = inspect.getsource(self._REGISTERED_MEMBERS[element.name])
			assert element.module_name in impl, "Method for element {} not properly namespaced using module {}".format(element.name, element.module_name)
			assert element.func.__name__ in impl, "Method for element {} does not call associated function {}".format(element.name, element.func.__name__)

		except OSError as e:
			print('Failed to get source implementation for registered element: {}'.format(element.name))
			pass

#!/usr/bin/env python3
#
# Copyright (C) 2019-2020  Hiroaki Ohta
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot
pyplot.style.use('classic')
matplotlib.rcParams.update({"text.usetex": True})
pyplot.rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
import numpy as np
from optparse import OptionParser
import pickle
import sqlite3

import lal
from lal import rate
from lal.utils import CacheEntry
from ligo.lw import dbtables
from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from gstlal import far
from gstlal import imr_utils
from gstlal import inspiral_pipe
from gstlal.plots import sensitivity as plotsens
from matplotlib import pyplot

@lsctables.use_in
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass

def parse_command_line():
	parser = OptionParser(description = __doc__)

	# FAR range and resolution
	parser.add_option("--min-far", metavar = "Hertz", default = 1.0e-6/lal.YRJUL_SI, type = "float", help = "Specify the minimum false-alarm rate in Hertz.  Default is 1 per million years.") # one per million years is probably detection worthy
	parser.add_option("--max-far", metavar = "Hertz", default = 12.0/lal.YRJUL_SI, type = "float", help = "Specify the maximum false-alarm rate in Hertz.  Default is 1 per month.") # one per month is possibly EM-followup worthy

	# Input data options
	parser.add_option("--injection-database", default = [], action = "append", help = "Name of database containing injection parameters and triggers (required if --check-vt is activated).")
	parser.add_option("--injection-files", default = [], action = "append", help = "XML files containing injection list (required if --check-vt is not activated).")
	parser.add_option("--instrument", action = "append", help = "Append to a list of instruments to create dist stats for.  List must be whatever instruments you intend to analyze.")
	parser.add_option("--lnlrcdf-cache", metavar = "url", help = "The urls of lnlrcdf_signal data.")
	parser.add_option("--ranking-stat-pdf", metavar = "filename", action = "append", help = "Load ranking statistic PDFs for the signal and noise models from this file (required).  The file must include the zero-lag count data.  This is typically in a file named \"post_marginalized_likelihood.xml.gz\".  Can be given multiple times.")
	parser.add_option("-c", "--check-vt", action = "store_true", help = "Compare VT results (optional).")

	# Output data options
	parser.add_option("--output-dir", default = "./", metavar = "name", help = "Select a directory to place output files.")
	parser.add_option("-v", "--verbose", action = "store_true", help = "Be verbose (optional).")

	options, filenames = parser.parse_args()

	required_options = ("instrument", "ranking_stat_pdf")
	if options.check_vt:
		required_options += ("injection_database",)
	else:
		required_options += ("injection_files",)

	missing_options = [option for option in required_options if not getattr(options, option)]
	if missing_options:
		raise ValueError("%s must be set" % ", ".join("--%s" % option.replace("_", "-") for option in missing_options))

	if options.lnlrcdf_cache:
		urls = [CacheEntry(line).url.replace("file://localhost", "") for line in open(options.lnlrcdf_cache)]
	else:
		urls = []
	urls.extend(filenames)
	if not urls:
		raise ValueError("no input documents")

	return options, urls

###########
#HARD CORD#
###########

mbins = [0.5, 2.0, 4.5, 45., 450]
observer = "ALL"

##############
#MAIN PROGRAM#
##############

options, urls = parse_command_line()

#
# create output directory if needed
#

os.makedirs(options.output_dir, exist_ok=True)

#
# Read caluculation data
#
if options.verbose:
	print("reading calculation data")

injs, inj_files = [], []
if options.check_vt:
	connection = {}
	for f in options.injection_database:
		# get injection tag from filename, supporting
		# both old and new filename convention
		if "GSTLAL_TRIGGER_DATABASE" in f:
			inj_file = CacheEntry.from_T050017(f).description.rpartition(f"GSTLAL_TRIGGER_DATABASE")[2].lstrip("_")
		else:
			inj_file = f.split("-")[1].replace("ALL_LLOID_","")
		inj_files.append(inj_file)
		connection[inj_file] = sqlite3.connect(f)
		injs += lsctables.SimInspiralTable.get_table(dbtables.get_xml(connection[inj_file]))
else:
	for injection_file in options.injection_files:
		inj_files.append(inspiral_pipe.sim_tag_from_inj_file(injection_file))
		injs += lsctables.SimInspiralTable.get_table(ligolw_utils.load_filename(injection_file, contenthandler = LIGOLWContentHandler, verbose = options.verbose))

distance_mchirps = [tup for tup in map(imr_utils.sim_to_distance_chirp_mass_bins_function, injs)]
if min(list(zip(*distance_mchirps))[1]) < mbins[0]:
	mbins[0] = min(list(zip(*distance_mchirps))[1])
if max(list(zip(*distance_mchirps))[1]) > mbins[-1]:
	mbins[-1] = max(list(zip(*distance_mchirps))[1])
bins = rate.NDBins([rate.LinearBins(min(list(zip(*distance_mchirps))[0]), max(list(zip(*distance_mchirps))[0]), 200), rate.IrregularBins(mbins)])
del distance_mchirps

total_injections = [tuple(((inj.distance, inj.mchirp),)) for inj in injs]
found_injections = []
lnL_th = []
for url in urls:
	with open(url, "rb") as f:
		lnlrcdfsignals = pickle.load(f)
		found_injections.extend(lnlrcdfsignals["lnlrcdf"])
		if list(lnL_th):
			assert (lnL_th == lnlrcdfsignals["lnL_th"]).all()
		else:
			lnL_th = lnlrcdfsignals["lnL_th"]

assert len(total_injections) == len(found_injections), "There is a problem with %s. The number of injections analyzed dosen't match up."%(options.lnlrcdf_cache)

#
# scale lnL to false alarm rate
#
if options.verbose:
	print("scaling lnL to FAR")

rankingstatpdf = far.marginalize_pdf_urls(options.ranking_stat_pdf, "RankingStatPDF", verbose = options.verbose)
fapfar = far.FAPFAR(rankingstatpdf.new_with_extinction())
fars = np.array([fapfar.far_from_rank(val) for val in lnL_th])
mask = (fars >= options.min_far*0.1) & (fars <= options.max_far*10)
livetime_yr = fapfar.livetime/lal.YRJUL_SI

#
# Compute sensitivity
#
if options.verbose:
	print("computing sensitivity")

vols_lo_calc, vols_calc, vols_hi_calc = [], [], []
for k, far in enumerate(fars):
	if not mask[k]:
		continue
	eff_lo, eff, eff_hi = imr_utils.compute_search_efficiency_in_bins(found_injections, total_injections, bins, sim_to_bins_function = lambda sim: sim[0], sim_to_found_weight_function = lambda sim: sim[1][k])
	vol_lo = imr_utils.compute_search_volume(eff_lo)
	vol_lo.array *= livetime_yr
	vols_lo_calc.append(vol_lo)

	vol = imr_utils.compute_search_volume(eff)
	vol.array *= livetime_yr
	vols_calc.append(vol)

	vol_hi = imr_utils.compute_search_volume(eff_hi)
	vol_hi.array *= livetime_yr
	vols_hi_calc.append(vol_hi)

fars = fars[mask]

if options.check_vt:
	#
	# Search injection data
	#
	if options.verbose:
		print("Searching injection data")

	found_injections = []
	total_injections = []
	for conn in connection.values():
		#found, total, missed = imr_utils.get_min_far_inspiral_injections(conn, segments = zero_lag_segments, table_name="coinc_inspiral")
		#found, total, missed = imr_utils.get_min_far_inspiral_injections(conn, segments = rankingstatpdf.segments, table_name="coinc_inspiral")
		found, total, missed = imr_utils.get_min_far_inspiral_injections(conn, table_name="coinc_inspiral")
		found_injections += found
		total_injections += total
	total_injections = imr_utils.symmetrize_sims(total_injections, "mass1", "mass2")

	vols_lo_far, vols_far, vols_hi_far = [], [], []
	for far in fars:
		found_by_far = [s for f, s in found_injections if f < far]
		eff_lo, eff, eff_hi = imr_utils.compute_search_efficiency_in_bins(found_by_far, total_injections, bins, imr_utils.sim_to_distance_chirp_mass_bins_function)
		vol_lo = imr_utils.compute_search_volume(eff_lo)
		vol_lo.array *= livetime_yr
		vols_lo_far.append(vol_lo)

		vol = imr_utils.compute_search_volume(eff)
		vol.array *= livetime_yr
		vols_far.append(vol)

		vol_hi = imr_utils.compute_search_volume(eff_hi)
		vol_hi.array *= livetime_yr
		vols_hi_far.append(vol_hi)

#
# plot injection and calculation result
#
if options.verbose:
	print("plotting calculation and injection results")

if len(inj_files) > 1:
	inj_files = ["COMBINED"]

far_vols_calc = (vols_lo_calc, vols_calc, vols_hi_calc)
if options.check_vt:
	from gstlal.plots import util as plotutil
	far_vols = (vols_lo_far, vols_far, vols_hi_far)
	mbins = rate.NDBins(bins[1:])
	for n, ((lo_calc, center_calc, hi_calc, label_calc), (lo, center, hi, label)) in enumerate(zip(plotsens.volumes_bins_to_range_label(far_vols_calc, mbins, "Source_Type"), plotsens.volumes_bins_to_range_label(far_vols, mbins, "Source_Type"))):
		fig_far = pyplot.figure()
		fig_far.set_size_inches((8., 8./plotutil.golden_ratio))
		ax_far = fig_far.gca()

		#calculation
		line, = ax_far.plot(fars, center_calc, label="Estimate Result : "+label, linewidth=2)
		ax_far.fill_between(fars, lo_calc, hi_calc, alpha=0.5, color=line.get_color())

		#injection
		line, = ax_far.plot(fars, center, label="Injection Result : "+label, linewidth=2)
		ax_far.fill_between(fars, lo, hi, alpha=0.5, color=line.get_color())

		ax_far.set_xlabel("Combined FAR (Hz)")
		ax_far.set_ylabel(r"Volume $\times$ Time ($\mathrm{Mpc}^3 \mathrm{yr}$)")
		ax_far.set_xscale("log")
		ax_far.set_yscale("log")
		ax_far.set_xlim(options.min_far, options.max_far)
		ax_far.invert_xaxis()
		ax_far.legend(loc="lower left")
		ax_far.grid()

		vol_tix = ax_far.get_yticks()
		tx = ax_far.twinx() # map volume to distance
		tx.set_yticks(vol_tix)
		tx.set_yscale("log")
		tx.set_ylim(ax_far.get_ylim())
		tx.set_yticklabels(["%.3g" % (plotsens.vt_to_range(float(k), livetime_yr)) for k in vol_tix])
		tx.set_ylabel("Range (Mpc)")

		ax_far.set_title("%s Observing (%.2f days)" % (observer, livetime_yr*365.25))
		fig_far.tight_layout(pad = .8)
		tag = "%s/%s_%s-SEARCH_SENSITIVITY_%s_CHECK_%s.png" %(options.output_dir, "".join(sorted(options.instrument)), observer, inj_files[0], n)
		fig_far.savefig(tag)
		pyplot.close(fig_far)

else:
	fig_far = plotsens.plot_sensitivity_vs_far(far_vols_calc, fars, livetime={observer:livetime_yr}, ifos=observer, bins=bins, bin_type="Source_Type")
	fig_far.axes[0].set_xlim(options.max_far, options.min_far)
	tag = "%s/%s_%s-SEARCH_SENSITIVITY_%s.png" %(options.output_dir, "".join(sorted(options.instrument)), observer, inj_files[0])
	fig_far.savefig(tag)
	pyplot.close(fig_far)

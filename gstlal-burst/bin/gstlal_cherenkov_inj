#!/usr/bin/env python3
#
# Copyright (C) 2021  Soichiro Kuwahara
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Cherenkov burst injection tool"""

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


import math
from optparse import OptionParser
import random
import sys

import lal

from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from ligo.lw.utils import process as ligolw_process


@lsctables.use_in
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass


__author__ = "Soichrio Kuwahara <soichiro.kuwahara@ligo.org>"

program_name = "gstlal_cherenkov_inj"


#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


def parse_command_line():
	parser = OptionParser(
		description = "GstLAL-based cherenkov burst injection pipeline."
	)
	parser.add_option("--beta", type = "float", help = "Set beta.")
	parser.add_option("--beta-width", type = "float", default = 0, help = "Set beta width for random input parameters (Optional).")
	parser.add_option("--f-natural", metavar = "Hz", type = "float", help = "Set f_natural.")
	parser.add_option("--f-width", metavar = "Hz", default = 0, type = "float", help = "Set f width for random input parameters (Optional)")
	parser.add_option("--output", metavar = "filename", help = "Set the name of the output file (default = stdout).")
	parser.add_option("--time-slide-file", metavar = "filename", help = "Associate injections with the first time slide ID in this XML file (required).")
	parser.add_option("--gps-geocent-time", metavar = "s", type = "int", help = "Set the start time of the tiling in GPS seconds (required).")
	parser.add_option("--deltaT", metavar = "s", default = 0, type = "int", help = "Set the time interval of injections. Default value is 0 for injecting only one.")
	parser.add_option("--num", type = "int", default = 1, help = "Set the number of injections. Default value is 1.")
	parser.add_option("--verbose", action = "store_true", help = "Be verbose.")
	options, filenames = parser.parse_args()

	# save for the process_params table
	options.options_dict = dict(options.__dict__)

	# check for params
	# FIXME: gps seconds and nanoseconds divided, error checking whether the waveform can be made is missing.
	if options.gps_geocent_time is None:
		raise ValueError("missing required option --gps-geocent-time")
	required_options = set(("output", "time_slide_file", "gps_geocent_time", "beta", "f_natural"))
	missing_options = set(option for option in required_options if getattr(options, option) is None)
	if missing_options:
		raise ValueError("missing required option(s) %s" % ", ".join("--%s" % options.subst("_", "_") for option in (requied_options - missing_options)))

	# type-cast
	# options.gps_geocent_time = lal.LIGOTimeGPS(options.gps_geocent_time)

	return options, filenames


#
# =============================================================================
#
#                                     Main
#
# =============================================================================
#


options, filenames = parse_command_line()


#
# use the time-slide file to start the output document
#


xmldoc = ligolw_utils.load_filename(options.time_slide_file, verbose = options.verbose, contenthandler = LIGOLWContentHandler)


#
# add our metadata
#


process = ligolw_process.register_to_xmldoc(xmldoc, "cherenkov_burst_inj", options.options_dict)


#
# use whatever time slide vector comes first in the table (lazy)
#


time_slide_table = lsctables.TimeSlideTable.get_table(xmldoc)
time_slide_id = time_slide_table[0].time_slide_id
if options.verbose:
	print("associating injections with time slide (%d) %s" % (time_slide_id, time_slide_table.as_dict()[time_slide_id]), file = sys.stderr)


#
# find or add a sim_burst table
#


try:
	lsctables.SimBurstTable.get_table(xmldoc)
except ValueError:
	# no sim_burst table in document
	pass
else:
	raise ValueError("%s contains a sim_burst table.  this program isn't smart enough to deal with that." % options.time_slide_xml)

sim_burst_tbl = xmldoc.childNodes[-1].appendChild(lsctables.New(lsctables.SimBurstTable, ["process:process_id", "simulation_id", "time_slide:time_slide_id", "waveform", "waveform_number", "ra", "dec", "psi", "q", "hrss", "time_geocent_gps", "time_geocent_gps_ns", "time_geocent_gmst", "duration", "frequency", "bandwidth", "egw_over_rsquared", "amplitude", "pol_ellipse_angle", "pol_ellipse_e"]))


#
# populate the sim_burst table with injections
#

for i in range(options.num):
	sim_burst_tbl.append(sim_burst_tbl.RowType(
		# metadata
		process_id = process.process_id,
		simulation_id = sim_burst_tbl.get_next_id(),
		time_slide_id = time_slide_id,
		waveform = "Cherenkov",

		# waveform parameters
		time_geocent = lal.LIGOTimeGPS(int(options.gps_geocent_time) + int(i * options.deltaT)),
		frequency = options.f_natural + random.uniform(-options.f_width, options.f_width),
		bandwidth = options.beta + random.uniform(-options.beta_width, options.beta_width),
		# Create the uniform and random amplitude in log10.
		egw_over_rsquared = pow(10, random.uniform(-1, 2)),

		# FIXME.  sky location and polarization axis orientation
		ra = 0.,
		dec = 0.,
		psi = 0.,

		# unnecessary columns
		waveform_number = 0.,
		amplitude = math.nan,
		duration = math.nan,
		q = math.nan,
		hrss = math.nan,
		pol_ellipse_angle = math.nan,
		pol_ellipse_e = math.nan
	))


#
# write output
#


ligolw_utils.write_filename(xmldoc, options.output, verbose = options.verbose)
